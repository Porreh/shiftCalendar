class Calendar {
	constructor() {
		this.html;
	}

	generateHTML() {
		const currentDate = new Date();
		let html = '';
		let labelDay = ['ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ', 'ВС'];
		let labelMonths = ['Январь', 'Февраль', 'Март', 'Апрель',
                    'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь',
                    'Октябрь', 'Ноябрь', 'Декабрь'];

		function getDaysInMonth(month, year) {
			const maxDays = new Date(year, month, 0);
			return maxDays.getDate();
		}

		function genMont(month) {
			html += `<div class="month_name">${labelMonths[month]}</div>`;
		}

		function genWeek() {
			html += '<div class="week">';
			for (let i = 0; i < 7; i++) {
				html += `<div class="week_name">${labelDay[i]}</div>`;
			}
			html += '</div>';
		}

		function genDays(month) {
			let firstDay = new Date(currentDate.getFullYear(), month, 0)
				.getDay();
			let monthLength = getDaysInMonth(month + 1, currentDate.getFullYear());
			let today = currentDate.getDate();
			let day = 1;

			html += '<div class="days">';
			for (let i = 0; i < 6; i++) {
				for (let j = 0; j < 7; j++) {
					if (day <= monthLength && (i > 0 || j >= firstDay)) {
						if (day === today && month === currentDate.getMonth()) {
							html += `<div id="${month + 1}&${day}" day="${day}" class="day s today">`;
						} else {
							html += `<div id="${month + 1}&${day}" day="${day}" class="day s">`;
						}
					} else {
						html += '<div class="day clear">';
					}
					if (day <= monthLength && (i > 0 || j >= firstDay)) {
						html += day;
						day++;
					}
					html += '</div>';
				}
				if (day > monthLength) {
					break;
				}
			}
			html += '</div>';
		}

		function generate() {
			html += '<div class="calendar noselect">';
			html += '<div class="board">';
			for (let i = 0; i < 12; i++) {
				html += '<div class="month">';
				genMont(i);
				genWeek();
				genDays(i);
				html += '</div>';
			}
			html += '</div></div>';
			return html;
		}

		this.html = generate();
	}

	render(element) {
		this.generateHTML();
		document.querySelector(element)
			.innerHTML = this.html;
	}
}

class Shift {
	createShift() {
		const listID = [];
		Array.from(document.querySelectorAll(".s"))
			.forEach(element => listID.push(element.getAttribute('id')));

		let gradeDown = index => {
			let tmpArray = [];
			while (index >= 0) {
				tmpArray.push(listID[index]);
        index -= 15;
			}
			return tmpArray;
		};

		let gradeUp = index => {
			let tmpArray = [];
			while (index < listID.length) {
				tmpArray.push(listID[index]);
        index += 15;
			}
			return tmpArray;
		};

		let reShift = (prev, next) => {
			let shiftArray = [];
			db.forEach(dbElement => {
				let index = listID.findIndex(x => dbElement == x);
				if ((index - prev) < 0) {
					index += next;
				} else {
          index -= prev;
        }
        if (prev === next) {
          shiftArray.push(listID[index]);
        }
				gradeDown(index)
					.forEach(id => shiftArray.push(id));
				gradeUp(index)
					.forEach(id => shiftArray.push(id));
			});
			return shiftArray;
		};

		let shift = () => {
			let classes = ['nightshift', 'dayshift', 'middleshift'];
			listID.forEach(id => {
				for (let i = 0; i < classes.length; i++) {
					document.getElementById(id)
						.classList.remove(classes[i]);
				}
			});

			reShift(15, 15)
				.forEach(id => {
					document.getElementById(id)
						.classList.add(classes[0]);
				});
			reShift(5, 10)
				.forEach(id => {
					document.getElementById(id)
						.classList.add(classes[1]);
				});
			reShift(10, 5)
				.forEach(id => {
					document.getElementById(id)
						.classList.add(classes[2]);
				});
		};

		shift();
	}

	watch(target) {
		let value = target.attributes.day.nodeValue;
		let id = target.id;
		if (db.find(x => id == x)) {
			let index = db.findIndex(x => id == x);
			db.splice(index, 1);
			console.info(`Deleted: ${value}.`);
			document.getElementById(id).classList.remove('selection');
		} else if (db.length === 3) {
			console.warn('Already created.');
		} else {
			db.push(id);
			console.info(`Added: ${value}.`);
			document.getElementById(id).classList.add('selection');
			if (db.length === 3) {
				this.createShift();
			}
		}
	}
}

const db = [];
const calendar = new Calendar();
const shift = new Shift();

calendar.render('.container');

let elements = Array.from(document.querySelectorAll('.s'));
for (let element of elements) {
	element.addEventListener('click', (e) => {
    shift.watch(e.target);
  });
}